package com.discover.security.model.token;

public interface JwtToken {
    String getToken();
}
