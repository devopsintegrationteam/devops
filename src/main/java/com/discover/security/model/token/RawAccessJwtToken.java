package com.discover.security.model.token;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;

import com.discover.security.config.JwtSettings;
import com.discover.security.config.WebSecurityConfig;
import com.discover.security.exceptions.JwtExpiredTokenException;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

public class RawAccessJwtToken implements JwtToken {
    private static Logger logger = LoggerFactory.getLogger(RawAccessJwtToken.class);
            
    private String token;
    
    public RawAccessJwtToken(String token) {
        this.token = token;
    }

    /**
     * Parses and validates JWT Token signature.
     * 
     * @throws BadCredentialsException
     * @throws JwtExpiredTokenException
     * 
     */
    public Jws<Claims> parseClaims(String signingKey) throws ExpiredJwtException {
        try {
            return Jwts.parser().setSigningKey(signingKey).parseClaimsJws(this.token);
        } catch (UnsupportedJwtException | MalformedJwtException | IllegalArgumentException | SignatureException ex) {
            logger.error("Invalid JWT Token", ex);
            throw new BadCredentialsException("Invalid JWT token: ", ex);
        } catch (ExpiredJwtException expiredEx) {
            logger.info("JWT Token is expired", expiredEx);
//            throw new JwtExpiredTokenException(this, "JWT Token expired", expiredEx);
            throw expiredEx;
        }
    }

    /**
     * Parses and validates JWT Token signature.
     * 
     * @throws BadCredentialsException
     * @throws JwtExpiredTokenException
     * 
     */
    public Jws<Claims> parseClaims(JwtSettings jwtSettings, String tokenParam) throws BadCredentialsException, ExpiredJwtException {
        try {
//        	long expireTime=0L;
//        	if (tokenParam == WebSecurityConfig.JWT_TOKEN_HEADER_PARAM) {
//        		Integer time = jwtSettings.getTokenExpirationTime();
//        		expireTime = new java.util.Date().getTime()+time;        		
//        	} else if  (tokenParam == WebSecurityConfig.JWT_TOKEN_HEADER_REFRESH_PARAM) {
//        		Integer time = jwtSettings.getRefreshTokenExpTime();
//        		expireTime = new java.util.Date().getTime()+time;
//        	} 
//        	return Jwts.parser().setSigningKey(jwtSettings.getTokenSigningKey()).requireExpiration(new Date(expireTime)).parseClaimsJws(this.token);
        	return Jwts.parser().setSigningKey(jwtSettings.getTokenSigningKey()).parseClaimsJws(this.token);
        } catch (UnsupportedJwtException | MalformedJwtException | IllegalArgumentException | SignatureException ex) {
            logger.error("Invalid JWT Token", ex);
            throw new BadCredentialsException("Invalid JWT token: ", ex);
        } catch (ExpiredJwtException expiredEx) {
            logger.info("JWT Token is expired", expiredEx);
//            throw new JwtExpiredTokenException(this, "JWT Token expired", expiredEx);
            throw expiredEx;
        }
    }
    @Override
    public String getToken() {
        return token;
    }
}
