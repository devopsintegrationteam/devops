package com.discover.security.model.token;

import java.util.List;
import java.util.Optional;

import org.springframework.security.authentication.BadCredentialsException;

import com.discover.security.config.JwtSettings;
import com.discover.security.exceptions.JwtExpiredTokenException;
import com.discover.security.model.Scopes;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;


@SuppressWarnings("unchecked")
public class RefreshToken implements JwtToken {
    private Jws<Claims> claims;
    RawAccessJwtToken token;
    
    private RefreshToken(RawAccessJwtToken token, Jws<Claims> claims) {
    	this.token = token;
        this.claims = claims;
    }

    /**
     * Creates and validates Refresh token 
     * 
     * @param token
     * @param signingKey
     * 
     * @throws BadCredentialsException
     * @throws JwtExpiredTokenException
     * 
     * @return
     */
    public static Optional<RefreshToken> create(RawAccessJwtToken token, String signingKey) {
        Jws<Claims> claims = token.parseClaims(signingKey);
        List<String> scopes = claims.getBody().get("scopes", List.class);
        if (scopes == null || scopes.isEmpty() 
                || !scopes.stream().filter(scope -> Scopes.REFRESH_TOKEN.authority().equals(scope)).findFirst().isPresent()) {
            return Optional.empty();
        }

        return Optional.of(new RefreshToken(token, claims));
    }

    public static Optional<RefreshToken> create(RawAccessJwtToken token, JwtSettings jwtSettings, String tokenParam) {
        Jws<Claims> claims = token.parseClaims(jwtSettings, tokenParam);
        List<String> scopes = claims.getBody().get("scopes", List.class);
        if (scopes == null || scopes.isEmpty() 
                || !scopes.stream().filter(scope -> Scopes.REFRESH_TOKEN.authority().equals(scope)).findFirst().isPresent()) {
            return Optional.empty();
        }

        return Optional.of(new RefreshToken(token, claims));
    }
    @Override
    public String getToken() {
    	/*
    	 * Shamil
    	 */
 //       return (this.token!=null? this.token.getToken(): null);
    	return null;
    }

    public Jws<Claims> getClaims() {
        return claims;
    }
    
    public String getJti() {
        return claims.getBody().getId();
    }
    
    public String getSubject() {
        return claims.getBody().getSubject();
    }
}
