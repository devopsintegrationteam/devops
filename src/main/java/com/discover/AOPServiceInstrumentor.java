package com.discover;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect 
@Component 
public class AOPServiceInstrumentor {
	 
	 private final Logger logger = LoggerFactory.getLogger(this.getClass());


	 	@AfterReturning("execution(* com.discover..*.*(..)) ")
	 					// execution(* com.spring.mvc.sha.beans.*.*(..))
		public void logServiceAccess(JoinPoint joinPoint) { 
	 		logger.error("This is an error message AOPServiceInstrumentor "+joinPoint);
	 		logger.debug("This is an error message AOPServiceInstrumentor "+joinPoint);
	 	}  
}