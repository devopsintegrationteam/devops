package com.discover.security;

import java.util.Optional;

import com.discover.entity.User;

public interface UserService {
    public Optional<User> getByUsername(String username);
}
