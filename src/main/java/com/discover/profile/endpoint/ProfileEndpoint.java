package com.discover.profile.endpoint;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.discover.security.auth.JwtAuthenticationToken;
import com.discover.security.config.WebSecurityConfig;
import com.discover.security.model.UserContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class ProfileEndpoint {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final ObjectMapper mapper = new ObjectMapper();
	
	@RequestMapping(value="/api/me", method=RequestMethod.GET, produces={ MediaType.APPLICATION_JSON_VALUE })
    public @ResponseBody UserContext get(JwtAuthenticationToken token) {
        return (UserContext) token.getPrincipal();
    }
   
    @RequestMapping(value="/servicesapi/noauth/*", method={RequestMethod.GET, RequestMethod.POST}, produces={ MediaType.APPLICATION_JSON_VALUE })
    public @ResponseBody JsonNode redirectToAPIServer(@RequestBody(required=false) JsonNode node, HttpServletRequest request, HttpServletResponse response) throws IOException{
    	RestTemplate restTemplate = new RestTemplate();
    	JsonNode jSonString = null;
        if (request.getMethod().equalsIgnoreCase("GET")) {
        	 jSonString = restTemplate.getForObject(WebSecurityConfig.REDIRECT_URL+request.getRequestURI(), JsonNode.class);
        } else if (request.getMethod().equalsIgnoreCase("POST")) {
  //      	 restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        	 jSonString = restTemplate.postForObject(WebSecurityConfig.REDIRECT_URL+request.getRequestURI(), node, JsonNode.class);
        } 
       
//        response.setStatus(HttpStatus.OK.value());
//        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
//        mapper.writeValue(response.getWriter(), jSonString);
//        response.getWriter().close();
        return jSonString;
        
    }
}
