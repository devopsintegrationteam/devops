package com.discover.security.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "app.security.jwt")
public class JwtSettings {
    /**
     * {@link JwtToken} will expire after this time.
     */
    private Integer tokenExpirationTime;

    /**
     * Token issuer.
     */
    private String tokenIssuer;
    
    /**
     * Key is used to sign {@link JwtToken}.
     */
    private String tokenSigningKey;
    
    /**
     * {@link JwtToken} can be refreshed during this timeframe.
     */
    private Integer refreshTokenExpTime;
    
	private String tokenRenewalType;
    private Integer  tokenRenewalRetry;
    private boolean  refreshTokenRequired;
    private String  refreshTokenRenewalType;
    private Integer  refreshTokenRetry;
    
    public String getTokenRenewalType() {
		return tokenRenewalType;
	}

	public void setTokenRenewalType(String tokenRenewalType) {
		this.tokenRenewalType = tokenRenewalType;
	}

	public Integer getTokenRenewalRetry() {
		return tokenRenewalRetry;
	}

	public void setTokenRenewalRetry(Integer tokenRenewalRetry) {
		this.tokenRenewalRetry = tokenRenewalRetry;
	}

	public boolean isRefreshTokenRequired() {
		return refreshTokenRequired;
	}

	public void setRefreshTokenRequired(boolean refreshTokenRequired) {
		this.refreshTokenRequired = refreshTokenRequired;
	}

	public String getRefreshTokenRenewalType() {
		return refreshTokenRenewalType;
	}

	public void setRefreshTokenRenewalType(String refreshTokenRenewalType) {
		this.refreshTokenRenewalType = refreshTokenRenewalType;
	}

	public Integer getRefreshTokenRetry() {
		return refreshTokenRetry;
	}

	public void setRefreshTokenRetry(Integer refreshTokenRetry) {
		this.refreshTokenRetry = refreshTokenRetry;
	}

    public Integer getRefreshTokenExpTime() {
        return refreshTokenExpTime;
    }

    public void setRefreshTokenExpTime(Integer refreshTokenExpTime) {
        this.refreshTokenExpTime = refreshTokenExpTime;
    }

    public Integer getTokenExpirationTime() {
        return tokenExpirationTime;
    }
    
    public void setTokenExpirationTime(Integer tokenExpirationTime) {
        this.tokenExpirationTime = tokenExpirationTime;
    }
    
    public String getTokenIssuer() {
        return tokenIssuer;
    }
    public void setTokenIssuer(String tokenIssuer) {
        this.tokenIssuer = tokenIssuer;
    }
    
    public String getTokenSigningKey() {
        return tokenSigningKey;
    }
    
    public void setTokenSigningKey(String tokenSigningKey) {
        this.tokenSigningKey = tokenSigningKey;
    }
}
