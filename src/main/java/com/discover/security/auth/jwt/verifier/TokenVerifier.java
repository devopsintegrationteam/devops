package com.discover.security.auth.jwt.verifier;

public interface TokenVerifier {
    public boolean verify(String jti);
}
