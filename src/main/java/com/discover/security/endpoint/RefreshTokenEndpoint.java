package com.discover.security.endpoint;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.discover.entity.User;
import com.discover.profile.endpoint.ProfileEndpoint;
import com.discover.security.UserService;
import com.discover.security.auth.jwt.extractor.JwtHeaderTokenExtractor;
import com.discover.security.auth.jwt.extractor.TokenExtractor;
import com.discover.security.auth.jwt.verifier.TokenVerifier;
import com.discover.security.config.JwtSettings;
import com.discover.security.config.WebSecurityConfig;
import com.discover.security.exceptions.InvalidJwtToken;
import com.discover.security.model.UserContext;
import com.discover.security.model.token.AccessJwtToken;
import com.discover.security.model.token.JwtToken;
import com.discover.security.model.token.JwtTokenFactory;
import com.discover.security.model.token.RawAccessJwtToken;
import com.discover.security.model.token.RefreshToken;

import io.jsonwebtoken.ExpiredJwtException;

@RestController
public class RefreshTokenEndpoint {

    @Autowired private JwtTokenFactory tokenFactory;
    @Autowired private JwtSettings jwtSettings;
    @Autowired private UserService userService;
    @Autowired private TokenVerifier tokenVerifier;
    @Autowired @Qualifier("jwtHeaderTokenExtractor") private TokenExtractor tokenExtractor;
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    
    private AccessJwtToken refreshToken(HttpServletRequest request, HttpServletResponse response, String tokenParam ) throws ExpiredJwtException, IOException, ServletException {
    	String tokenPayload = tokenExtractor.extract(request.getHeader(tokenParam));
        RawAccessJwtToken rawToken = new RawAccessJwtToken(tokenPayload);
        RefreshToken refreshToken = null;
        try {
        	refreshToken = RefreshToken.create(rawToken, jwtSettings, tokenParam).orElseThrow(() -> new InvalidJwtToken());
        }catch(ExpiredJwtException e) {
        	logger.error("Trying to authorize using X-Refresh");
        	tokenPayload = tokenExtractor.extract(request.getHeader(WebSecurityConfig.JWT_TOKEN_HEADER_REFRESH_PARAM));
            rawToken = new RawAccessJwtToken(tokenPayload);
        	refreshToken = RefreshToken.create(rawToken, jwtSettings, tokenParam).orElseThrow(() -> new InvalidJwtToken());
        }

        String jti = refreshToken.getJti();
        if (!tokenVerifier.verify(jti)) {
            throw new InvalidJwtToken();
        }

        String subject = refreshToken.getSubject();
        User user = userService.getByUsername(subject).orElseThrow(() -> new UsernameNotFoundException("User not found: " + subject));

        if (user.getRoles() == null) throw new InsufficientAuthenticationException("User has no roles assigned");
        List<GrantedAuthority> authorities = user.getRoles().stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getRole().authority()))
                .collect(Collectors.toList());
        UserContext userContext = UserContext.create(user.getUsername(), authorities);
        AccessJwtToken accessToken = null;
        if (tokenParam == WebSecurityConfig.JWT_TOKEN_HEADER_REFRESH_PARAM) {
        	accessToken = tokenFactory.createRefreshToken(userContext);
        } else if (tokenParam == WebSecurityConfig.JWT_TOKEN_HEADER_PARAM) {
        	accessToken = tokenFactory.createAccessJwtToken(userContext);
        } 
        response.setHeader(tokenParam, JwtHeaderTokenExtractor.HEADER_PREFIX.concat(accessToken.getToken()));
        response.setStatus(HttpStatus.OK.value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        logger.error(tokenParam+"  "+accessToken.getToken());
        return accessToken;    	
    }
    
    @RequestMapping(value="/api/auth/token", method=RequestMethod.GET, produces={ MediaType.APPLICATION_JSON_VALUE })
    public @ResponseBody AccessJwtToken[] refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    	AccessJwtToken[] jwtToken = new AccessJwtToken[2];
    	
        if (this.jwtSettings.isRefreshTokenRequired()) {
            if (this.jwtSettings.getRefreshTokenRenewalType().equals("automatic")) {
            	jwtToken[0]=this.refreshToken(request, response, WebSecurityConfig.JWT_TOKEN_HEADER_REFRESH_PARAM);            	
            }
        } 
        if (this.jwtSettings.isRefreshTokenRequired()) {
        	if (this.jwtSettings.getRefreshTokenRenewalType().equals("automatic")) {
        	jwtToken[1]=this.refreshToken(request, response, WebSecurityConfig.JWT_TOKEN_HEADER_PARAM);
        	}
        } else {
        	jwtToken[1]=this.refreshToken(request, response, WebSecurityConfig.JWT_TOKEN_HEADER_PARAM);
        	}
        
       	
       	logger.error("X-Auth-Refresh Details : "+jwtToken[0].getClaims());
       	logger.error("X-Refresh Details      : "+jwtToken[1].getClaims());
       	return jwtToken;
    }
}



//String tokenPayload = tokenExtractor.extract(request.getHeader(WebSecurityConfig.JWT_TOKEN_HEADER_PARAM));
//RawAccessJwtToken rawToken = new RawAccessJwtToken(tokenPayload);
//RefreshToken refreshToken = RefreshToken.create(rawToken, jwtSettings.getTokenSigningKey()).orElseThrow(() -> new InvalidJwtToken());
////     AccessJwtToken refreshToken = RefreshToken.create(rawToken, jwtSettings.getTokenSigningKey()).orElseThrow(() -> new InvalidJwtToken());
//String jti = refreshToken.getJti();
//if (!tokenVerifier.verify(jti)) {
//    throw new InvalidJwtToken();
//}
//
//String subject = refreshToken.getSubject();
//User user = userService.getByUsername(subject).orElseThrow(() -> new UsernameNotFoundException("User not found: " + subject));
//
//if (user.getRoles() == null) throw new InsufficientAuthenticationException("User has no roles assigned");
//List<GrantedAuthority> authorities = user.getRoles().stream()
//        .map(authority -> new SimpleGrantedAuthority(authority.getRole().authority()))
//        .collect(Collectors.toList());
//UserContext userContext = UserContext.create(user.getUsername(), authorities);
//AccessJwtToken accessToken = tokenFactory.createAccessJwtToken(userContext);
///*
//*  Shamil Testing Starts
//*/
////logger.error("HTTP Response : "+response);
////logger.error("Refresh Token : "+refreshToken);
////logger.error("Refresh Token getToken : "+accessToken.getToken());
////Ends        
//response.setHeader(com.discover.security.config.WebSecurityConfig.JWT_TOKEN_HEADER_PARAM, JwtHeaderTokenExtractor.HEADER_PREFIX.concat(accessToken.getToken()));
//response.setStatus(HttpStatus.OK.value());
//response.setContentType(MediaType.APPLICATION_JSON_VALUE);
//return accessToken;