package com.discover.security.auth.ajax;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.discover.security.auth.jwt.extractor.JwtHeaderTokenExtractor;
import com.discover.security.config.JwtSettings;
import com.discover.security.model.UserContext;
import com.discover.security.model.token.AccessJwtToken;
import com.discover.security.model.token.JwtToken;
import com.discover.security.model.token.JwtTokenFactory;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class AjaxAwareAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    private final ObjectMapper mapper;
    private final JwtTokenFactory tokenFactory;
    private final JwtSettings jwtSettings;
    
    @Autowired
    public AjaxAwareAuthenticationSuccessHandler(final ObjectMapper mapper, final JwtTokenFactory tokenFactory, final JwtSettings jwtSettings) {
        this.mapper = mapper;
        this.tokenFactory = tokenFactory;
        this.jwtSettings = jwtSettings;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
            Authentication authentication) throws IOException, ServletException {
        UserContext userContext = (UserContext) authentication.getPrincipal();
        
        AccessJwtToken accessToken = tokenFactory.createAccessJwtToken(userContext);
        JwtToken refreshToken = tokenFactory.createRefreshToken(userContext);
        
        Map<String, String> tokenMap = new HashMap<String, String>();
        tokenMap.put("token", accessToken.getToken());
        
        response.setHeader(com.discover.security.config.WebSecurityConfig.JWT_TOKEN_HEADER_PARAM, JwtHeaderTokenExtractor.HEADER_PREFIX.concat(accessToken.getToken()));
        if (this.jwtSettings.isRefreshTokenRequired()) {
        	tokenMap.put("refreshToken", refreshToken.getToken());
        	response.setHeader(com.discover.security.config.WebSecurityConfig.JWT_TOKEN_HEADER_REFRESH_PARAM, JwtHeaderTokenExtractor.HEADER_PREFIX.concat(refreshToken.getToken()));
        }
        response.setStatus(HttpStatus.OK.value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        mapper.writeValue(response.getWriter(), tokenMap);

        clearAuthenticationAttributes(request);
    }

    /**
     * Removes temporary authentication-related data which may have been stored
     * in the session during the authentication process..
     * 
     */
    protected final void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);

        if (session == null) {
            return;
        }

        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }
}
